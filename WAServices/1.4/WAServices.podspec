Pod::Spec.new do |s|
s.name             = "WAServices"
s.version          = "1.4"
s.summary          = "Base API for Walkabout Apps based on dialect of Eve python api server."
s.homepage         = "https://walkaboutapps.wix.com/apps"
s.license          = { :type => 'BSD' }
s.author           = { "walkabout" => "walkaboutapps@gmail.com" }
s.source           = { :git => "git@bitbucket.org:walkaboutapps/waservices.git", :tag => s.version }
s.social_media_url = 'https://twitter.com/artsy'

s.platform     = :ios, '9.0'
s.requires_arc = true

s.source_files = 'WAServices/*'
s.module_name = 'WAServices'

s.dependency 'AlamofireObjectMapper'
s.dependency 'AlamofireNetworkActivityIndicator', '~> 2.0'
s.dependency 'ObjectMapper'

end
